#!/usr/bin/env bash
# Exception described at https://github.com/koalaman/shellcheck/wiki/SC1091
# Connected with https://github.com/koalaman/shellcheck/wiki/SC1090

# shellcheck disable=SC1091
# shellcheck source=production_code_statistics.sh
source "$SCRIPT_DIR"/scripts/production_code_statistics.sh
# shellcheck disable=SC1091
# shellcheck source=test_code_statistics.sh
source "$SCRIPT_DIR"/scripts/test_code_statistics.sh
# shellcheck disable=SC1091
# shellcheck source=public_api_statistics.sh
source "$SCRIPT_DIR"/scripts/public_api_statistics.sh

statistic() {
  empty_line
  echo "== Code statistics"
  empty_line
  echo "Aggregates numbers about production and test code. Also, informs how many test there are and their status."
  production_code_stats
  test_code_stats
  public_api
}
