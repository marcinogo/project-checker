#!/usr/bin/env bash
inject_taglist_analysis() {
  #  TODO work on regexes for tags to get wired combinations
  cat <<'EOF' >taglist_analisys.xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.gitlab.marcinogo</groupId>
    <artifactId>project-checker</artifactId>
    <version>0.0.1</version>
    <name>project-checker</name>
    <url>https://gitlab.com/marcinogo/project-checker</url>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

        <project.java.version>17</project.java.version>
        <version.maven>3.8.4</version.maven>

        <maven.compiler.release>${project.java.version}</maven.compiler.release>
        <maven.compiler.encoding>${project.build.sourceEncoding}</maven.compiler.encoding>
        <maven.resources.encoding>${project.build.sourceEncoding}</maven.resources.encoding>

        <version.plugin.maven.enforcer>3.0.0</version.plugin.maven.enforcer>
        <version.plugin.maven.compiler>3.10.1</version.plugin.maven.compiler>

        <version.plugin.maven.taglist>3.0.0</version.plugin.maven.taglist>
    </properties>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-enforcer-plugin</artifactId>
                <version>${version.plugin.maven.enforcer}</version>
                <executions>
                    <execution>
                        <id>enforce-maven</id>
                        <goals>
                            <goal>enforce</goal>
                        </goals>
                        <configuration>
                            <rules>
                                <requireJavaVersion>
                                    <version>${project.java.version}</version>
                                </requireJavaVersion>
                                <requireMavenVersion>
                                    <version>${version.maven}</version>
                                </requireMavenVersion>
                            </rules>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${version.plugin.maven.compiler}</version>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>taglist-maven-plugin</artifactId>
                <version>${version.plugin.maven.taglist}</version>
                <configuration>
                    <tagListOptions>
                        <tagClasses>
                            <tagClass>
                                <displayName>@todo</displayName>
                                <tags>
                                    <tag>
                                        <matchString>@todo</matchString>
                                        <matchType>exact</matchType>
                                    </tag>
                                </tags>
                            </tagClass>
                            <tagClass>
                                <displayName>TODO</displayName>
                                <tags>
                                    <tag>
                                        <matchString>TODO</matchString>
                                        <matchType>ignoreCase</matchType>
                                    </tag>
                                </tags>
                            </tagClass>
                            <tagClass>
                                <displayName>FIXME</displayName>
                                <tags>
                                    <tag>
                                        <matchString>FIXME</matchString>
                                        <matchType>ignoreCase</matchType>
                                    </tag>
                                </tags>
                            </tagClass>
                        </tagClasses>
                    </tagListOptions>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
EOF
}

remove_injected_taglist_analysis() {
  rm taglist_analisys.xml
}

list_todos() {
  inject_taglist_analysis
  mvn clean test-compile -q
  mvn taglist:taglist -q -f taglist_analisys.xml
  empty_line
  echo "== TODOs in the project"
  empty_line
  echo "[source,xml]"
  echo "----"
  echo "include::target/taglist/taglist.xml[]"
  echo "----"
  empty_line
  echo "To see report in nicer format use commands \`mvn taglist:taglist\` and \
  \`firefox target/site/taglist.html\` with proper \`pom.xml\` configuration in the project's root location"
  remove_injected_taglist_analysis
}
