#!/usr/bin/env bash
git_analysis() {
  empty_line
  echo "== Git analysis"
  empty_line
  echo "Shows who worked on the repo, commit messages and git history overview."
  git_committers
  git_history_overview
  git_commit_interesting_messages
}

git_committers() {
  empty_line
  echo "=== Committers"
  opening_bash_code_block
  git shortlog -sne
  closing_bash_code_block
}

git_history_overview() {
  empty_line
  echo "=== Git history"
  opening_bash_code_block
  git log --graph --abbrev-commit --decorate \
    --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset)''
    %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n
    %C(white)%s%C(reset) %C(dim white)- %an%C(reset)' --all
  empty_line
  closing_bash_code_block
}

git_commit_interesting_messages() {
  empty_line
  echo "=== Interesting commits"
  empty_line
  echo "All commits with more then one line in a message."
  echo "If empty then there is no interesting messages."
  opening_bash_code_block
#  FIXME still passes oneliners
  git log --pretty=format:'%H<msgst>%b<msge>' |
    grep -F -v '<msgst><msge>' | grep -F '<msgst>' | cut -b1-40 |
    git log --stdin --no-walk --no-merges
  empty_line
  closing_bash_code_block
}
