#!/usr/bin/env bash
separator() {
  empty_line
  echo "'''"
  empty_line
}

empty_line() {
  echo ""
}

page_separator() {
  empty_line
  echo "<<<"
  empty_line
}

opening_bash_code_block() {
  empty_line
  echo "[source,bash]"
  echo "----"
}

closing_bash_code_block() {
  echo "----"
}
