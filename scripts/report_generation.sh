#!/usr/bin/env bash
# Exception described at https://github.com/koalaman/shellcheck/wiki/SC1091
# Connected with https://github.com/koalaman/shellcheck/wiki/SC1090

# shellcheck disable=SC1091
# shellcheck source=adoc_formatting.sh
source "$SCRIPT_DIR"/scripts/adoc_formatting.sh
# shellcheck disable=SC1091
# shellcheck source=sgit_analysis.sh
source "$SCRIPT_DIR"/scripts/git_analysis.sh
# shellcheck disable=SC1091
# shellcheck source=build_project.sh
source "$SCRIPT_DIR"/scripts/build_project.sh
# shellcheck disable=SC1091
# shellcheck source=pom_analysis.sh
source "$SCRIPT_DIR"/scripts/pom_analysis.sh
# shellcheck disable=SC1091
# shellcheck source=code_statistics.sh
source "$SCRIPT_DIR"/scripts/code_statistics.sh
# shellcheck disable=SC1091
# shellcheck source=static_code_analysis.sh
source "$SCRIPT_DIR"/scripts/static_code_analysis.sh
# shellcheck disable=SC1091
# shellcheck source=taglist_report.sh
source "$SCRIPT_DIR"/scripts/taglist_report.sh
# shellcheck disable=SC1091
# shellcheck source=jdeps_graphs.sh
source "$SCRIPT_DIR"/scripts/jdeps_graphs.sh

create_report_file() {
  cat <<EOF >report.adoc
:icons: font
:source-highlighter: pygments
:toc: preamble
:hardbreaks:
:data-uri:
:imagesdir: temp_img
= Report about $PROJECT_NAME

Aggregates information about the project. Based on Maven, scripts and static code analysis.
EOF
}

generate_report_adoc() {
  echo "Creating report.adoc"
  create_report_file
  {
    git_analysis
    build_project
    pom_analyse
    statistic
    jdeps_analysis
    static_code_analysis
    list_todos
  } >>report.adoc
}

generate_report_pdf() {
  if asciidoctor-pdf -v; then
    echo "Generating PDF based on report.adoc"
    asciidoctor-pdf report.adoc
    echo "Opening report.pdf"
    xdg-open report.pdf
    echo "Removing target directory"
    cleanup
  fi
}

generate_report() {
  generate_report_adoc
  generate_report_pdf
  echo "Report about $PROJECT_NAME had been generated"
}

cleanup() {
  mvn clean -q
  rm report.adoc
  remove_images
}
