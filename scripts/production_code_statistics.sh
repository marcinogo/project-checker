#!/usr/bin/env bash
number_of_java_files_main() {
  echo "There are $(find src/main -type f -name '*.java' | wc -l) Java files in production directories"
}

number_of_code_lines_main() {
  echo "Number of Java file lines in src/main is equal to \
  $(find ./src/main/java/ -type f -name '*.java' -exec cat {} \+ | wc -l)"
}

number_of_production_classes() {
  echo "There are $(find ./src/main/java/ -type f -name '*.java' -exec grep 'class' {} \+ | wc -l) \
  classes in production code"
}

number_of_production_enums() {
  echo "There are $(find ./src/main/java/ -type f -name '*.java' -exec grep 'enum' {} \+ | wc -l) \
  enum in production code"
}

number_of_production_interfaces() {
  echo "There are $(find ./src/main/java/ -type f -name '*.java' -exec grep 'interface' {} \+ | wc -l) \
  interfaces in production code"
}

number_of_production_records() {
  echo "There are $(find ./src/main/java/ -type f -name '*.java' -exec grep 'record' {} \+ | wc -l) \
records in production code"
}

packages() {
  echo "There are $(find src/main/java/* -type d | wc -l) packages"
  empty_line
  echo "Those are:"
  find src/main/java/* -type d | sed 's/\//./g' | sed 's/src.main.java.//g'
}

production_code_stats() {
  empty_line
  echo "=== Production code"
  empty_line
  echo "Shows statistic about packages and code in src/main"
  empty_line
  packages
  separator
  number_of_java_files_main
  number_of_code_lines_main
  separator
  number_of_production_classes
  number_of_production_enums
  number_of_production_records
  number_of_production_interfaces
}
