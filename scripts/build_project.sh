#!/usr/bin/env bash
build_project() {
  empty_line
  echo "== Building project"
  empty_line
  echo "Tries to build the project"
  opening_bash_code_block
  if mvn clean package >/dev/null; then
    echo "Build successful"
  else
    echo "Build unsuccessful"
  fi
  closing_bash_code_block
}
