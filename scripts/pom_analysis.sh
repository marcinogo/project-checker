#!/usr/bin/env bash
show_project_dependencies() {
  echo "Showing project's dependencies"
  opening_bash_code_block
  mvn -e org.apache.maven.plugins:maven-dependency-plugin:3.3.0:tree
  closing_bash_code_block
}

analise_project_dependencies() {
  echo "Analyzes the dependencies of this project and determines which are: \
  used and declared; used and undeclared; unused and declared."
  opening_bash_code_block
  mvn -e org.apache.maven.plugins:maven-dependency-plugin:3.3.0:analyze
  closing_bash_code_block
}

potential_pom_updates() {
  empty_line
  echo "Potential items in \`<properties>\` for update."
  opening_bash_code_block
  mvn versions:display-property-updates
  closing_bash_code_block
  page_separator
  echo "Potential dependencies for update."
  opening_bash_code_block
  mvn versions:display-dependency-updates
  closing_bash_code_block
  page_separator
  echo "Potential plugins for update."
  opening_bash_code_block
  mvn versions:display-plugin-updates
  closing_bash_code_block
}

# TODO make it useful
effective_pom() {
  empty_line
  echo "=== Effective pom"
  empty_line
  echo "Shows project's effective pom"
  opening_bash_code_block
  mvn help:effective-pom
  closing_bash_code_block
}

list_project_dependencies() {
  empty_line
  echo "=== Project's dependencie"
  empty_line
  show_project_dependencies
  analise_project_dependencies
}

pom_analyse() {
  empty_line
  echo "== pom.xml"
  empty_line
  echo "Analysing pom.xml for properties, dependencies and plugins updates"
  empty_line
  potential_pom_updates
  empty_line
  effective_pom
  empty_line
  list_project_dependencies
}
