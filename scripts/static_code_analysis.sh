#!/usr/bin/env bash
linting_code() {
  empty_line
  echo "=== Linting code"
  empty_line
  echo "Checks for usage of deprecated APIs"
  opening_bash_code_block
  mvn clean test-compile -Dmaven.compiler.showDeprecation -Dmaven.compiler.showWarnings -Dmaven.compiler.failOnWarning
  closing_bash_code_block
}

spotbugs_analysis() {
  empty_line
  page_separator
  empty_line
  echo "=== Static code analysis with Spotbugs"
  opening_bash_code_block
  mvn clean test-compile -q
  mvn -e com.github.spotbugs:spotbugs-maven-plugin:4.5.3.0:check
  closing_bash_code_block
}

static_code_analysis() {
  empty_line
  echo "== Static code analysis"
  empty_line
  linting_code
  spotbugs_analysis
}
