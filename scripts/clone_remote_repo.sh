#!/usr/bin/env bash
obtaining_remote_repo() {
  # Making sure that a given directory exists by creating it if necessary.
  # The default dire in user's home will be used if the 2nd parameter is not passed.
  create_dir_for_project "$@"
  # Clone the repo that is specified as 1st param.
  REPO_URL="$1"
  if [ "$#" -gt 2 ]; then
    # TODO: getopts to make the parameters place/index independent
    PROJECT_NAME="$3"
    clone_repo_rename "$REPO_URL" "$PROJECT_NAME"
  else
    clone_repo_no_rename "$REPO_URL"
  fi
}

create_dir_for_project() {
  CLONE_DEST="${2:-$HOME/project_checker}"
  echo "Making sure that $CLONE_DEST directory exists"
  mkdir -p "$CLONE_DEST"
  change_dir "$CLONE_DEST"
}

clone_repo_no_rename() {
  echo "Cloning $REPO_URL repository"
  if ! git clone "$REPO_URL"; then
    echo "Could not clone $REPO_URL"
  fi
}

clone_repo_rename() {
  echo "Cloning $REPO_URL repository to $PROJECT_NAME"
  if ! git clone "$REPO_URL" "$PROJECT_NAME"; then
    echo "Could not clone $REPO_URL"
  fi
}

extracting_remote_project_name() {
  echo "Extracting project's name from URL"
  REMOVE_PREFIX="${REPO_URL##*/}"
  PROJECT_NAME="${REMOVE_PREFIX%.*}"
}

change_dir() {
  DIR_PATH="$1"
  echo "Changing directory to $DIR_PATH"
  cd "$DIR_PATH" || {
    echo "Could not open destined directory"
    exit 1
  }
}

remote() {
  obtaining_remote_repo "$@"
  if [ -z "$PROJECT_NAME" ]; then
    extracting_remote_project_name
  fi
  change_dir "$PROJECT_NAME"
}
