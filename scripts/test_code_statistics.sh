#!/usr/bin/env bash
number_of_lines_test() {
  echo "Number of Java file lines in src/test is equal to \
  $(find ./src/test/java/ -type f -name '*.java' -exec cat {} \+ | wc -l)"
}

number_of_java_files_test() {
  echo "There are $(find src/test -type f -name '*.java' | wc -l) Java files in test directories"
}

number_of_java_files_unit_tests() {
  echo "There are $(find src/test -type f -name '*Test.java' | wc -l) unit tests following naming convention"
}

number_of_java_files_integration_tests() {
  echo "There are $(find src/test -type f -name '*IT.java' | wc -l) integration tests following naming convention"
}

unit_tests_results() {
  echo "Runs unit tests. If no statistics, then no tests present."
  mvn clean test | grep -A 5 "Results"
}

integration_tests_results() {
  echo "Runs integration tests. If no statistics, then no tests present."
  mvn clean test-compile failsafe:integration-test | grep -A 5 "Results"
}

test_code_stats() {
  empty_line
  echo "=== Test code"
  empty_line
  echo "Shows statistic about packages and code in src/test"
  number_of_java_files_test
  number_of_lines_test
  separator
  number_of_java_files_unit_tests
  unit_tests_results
  separator
  number_of_java_files_integration_tests
  integration_tests_results
}
