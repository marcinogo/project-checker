#!/usr/bin/env bash
removing_jar_from_dotfiles() {
  for f in "$PWD"/temp_img/*.dot; do
    sed -i -E 's/\s+\([[:alnum:][:punct:]]*.jar\)//' "$f"
  done
}

packages_map() {
  mvn clean package -DskipTests -q >/dev/null 2>&1
  jdeps -apionly -R -e "$BASE"'.*' -dotoutput "$PWD"/temp_img "$PWD"/target/*.jar
  removing_jar_from_dotfiles
  for d in "$PWD"/temp_img/*.dot; do
    if [[ "$d" == "$PWD/temp_img/summary.dot" ]]; then
      continue
    fi
    sfdp -Grotate=90 -Tpng "$d" -o "$PWD"/temp_img/sfdp-mp.png
    osage -Tpng "$d" -o "$PWD"/temp_img/osage-mp.png
  done
}

classes_map() {
  mvn clean package -DskipTests -q >/dev/null 2>&1
  jdeps -apionly -verbose:class -R -e "$BASE"'.*' -dotoutput "$PWD"/temp_img "$PWD"/target/*.jar
  removing_jar_from_dotfiles
  for d in "$PWD"/temp_img/*.dot; do
    if [[ "$d" == "$PWD/temp_img/summary.dot" ]]; then
      continue
    fi
    sfdp -Grotate=90 -Tpng "$d" -o "$PWD"/temp_img/sfdp-mc.png
    osage -Tpng "$d" -o "$PWD"/temp_img/osage-mc.png
  done
}

jdeps_analysis() {
  empty_line
  echo "== Dependencies graph"
  empty_line
  BASE=$(ls src/main/java)
  packages_map
  classes_map
  include_image "$PWD/temp_img/osage-mp.png" "Packages map"
  include_image "$PWD/temp_img/sfdp-mp.png" "Packages map - rotated"
  include_image "$PWD/temp_img/osage-mc.png" "Classes map"
  include_image "$PWD/temp_img/sfdp-mc.png" "Classes map - rotated"
}

include_image() {
  empty_line
  echo ".$2"
  echo "image::$1[\"$2\"]"
  empty_line
}

remove_images() {
  rm -r temp_img
}
