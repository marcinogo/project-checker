#!/usr/bin/env bash
public_api() {
  empty_line
  echo "=== Public API"
  empty_line
  packages
  empty_line
  echo "There are $(find src/main/java/ -type f -name '*.java' -exec grep -l 'public' {} \+ | wc -l) \
  files with public APIs (classes/enums/interfaces/records, fields and/or methods)"
  empty_line
  echo "Those are in <file>:<number>"
  find src/main/java/ -type f -name '*.java' -exec grep -c -H 'public' {} \; | grep -v ':0$'
}
