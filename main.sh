#!/usr/bin/env bash

# A script that checks a given project's quality.
# The first iteration to check what is necessary for the app to work.
# What can be done? What Java APIs are have to be checked?

SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)

# Exception described at https://github.com/koalaman/shellcheck/wiki/SC1091
# Connected with https://github.com/koalaman/shellcheck/wiki/SC1090

# shellcheck disable=SC1091
# shellcheck source=scripts/clone_remote_repo.sh
source "$SCRIPT_DIR"/scripts/clone_remote_repo.sh
# shellcheck disable=SC1091
# shellcheck source=scripts/report_generation.sh
source "$SCRIPT_DIR"/scripts/report_generation.sh

# TODO clean outputs and make them more useful
# TODO choose checks via parameters (getopts)
# TODO parse report xml to nice format
# FIXME showing only mulit line commits sometimes passes oneliners
run() {
  echo "Staring script $0"
  if [ "$#" -lt 1 ]; then
    echo "No URL for the remote repository given"
    echo "Analysing project in the current directory: $PWD"
  else
    echo "Found URL for the remote repository"
    remote "$@"
  fi
  if [ -z "$PROJECT_NAME" ]; then
    PROJECT_NAME="${PWD##*/}"
  fi
  generate_report
}

run "$@"
